import React from 'react'

import {
    View,
    Text,
    TouchableOpacity,
    Image,
    StyleSheet
} from 'react-native';
import { useNavigation } from "@react-navigation/native";

export const Header = ({ name, isback }) => {
    const navigation = useNavigation();
    const showDrawer = () => {

    }
    const back = () => {
        navigation.goBack()
    }
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() => isback ? back() : showDrawer()} style={styles.menu}>
                {isback ?
                    <Image

                        source={require('../assets/imag/back.png')}
                    />

                    :
                    <Image

                        source={require('../assets/imag/bars-solid.png')}
                    />
                }
            </TouchableOpacity>
            <View style={styles.headerName}>
                <Text style={styles.name}>{name}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 60,
        borderColor: "#C6C6C6",
        borderBottomWidth: 1,
        flexDirection: "row",
    },
    menu: {
        height: 60,
        width: "20%",
        position: "relative",
        zIndex: 99,
        justifyContent: "center",
        paddingLeft: 15
    },
    headerName: {
        height: 60,
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
        position: "absolute"
    },
    name: {
        fontSize: 17
    }
});
