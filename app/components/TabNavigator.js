import React from 'react'
import {
    View,
    Image,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import { useNavigation } from "@react-navigation/native";
export const TabNavigator = () => {
    const navigation = useNavigation();
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.optionOne}>
                <Image
                    source={require('../assets/imag/usuario.png')}
                />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate("create")} style={styles.optionTwo} >
                <Image
                    source={require('../assets/imag/mas.png')}
                />
            </TouchableOpacity>
            <TouchableOpacity  onPress={() => navigation.navigate("home")} style={styles.optionThree}>
                <Image
                    source={require('../assets/imag/torre.png')}
                />
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        height: 50,
        backgroundColor: "#FF4E4E",
        flexDirection: "row",
        alignItems: "center"
    },
    optionOne: {
        width: "33%",
        alignItems: "center"
    },
    optionTwo: {
        width: "33%",
        alignItems: "center",
        height: "60%",
        justifyContent: "center",
        borderColor: "#FFF",
        borderLeftWidth: 1,
        borderRightWidth: 1
    },
    optionThree: {
        width: "33%",
        alignItems: "center"
    }
});
