import React from 'react'
import {
    View,
    Dimensions,
} from 'react-native';
import { Users } from './Users'
import { TabNavigator } from './../TabNavigator'
export const Home = () => {

    const Dwidth = Dimensions.get("window").width
    const Dheight = Dimensions.get("window").height
    return (
        <View style={{ width: Dwidth, height: Dheight }}>
            <Users />
            <TabNavigator />
        </View>
    )
}
