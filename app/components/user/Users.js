import React, { useState, Fragment } from 'react';
import {
    View,
    TextInput,
    FlatList,
    Image,
    StyleSheet
} from 'react-native';
import { ItemUser } from './ItemUser'
import { data } from './../../data'
import { ScrollView } from 'react-native';
import { Header } from './../Header'

export const Users = () => {

    const [search, setSearch] = useState('');



    return (
        <Fragment>

            <Header name={"Usuarios"} />
            <ScrollView>
                <View style={styles.container}>
                    <View style={styles.containerSearch}>
                        <Image
                            style={styles.icon}
                            source={require('../../assets/imag/search-solid.png')}
                        />
                    </View>
                    <View style={styles.containerInput}>
                        <TextInput
                            onChangeText={text => setSearch(text)}
                            placeholderTextColor="#D5D5D5"
                            placeholder="Buscar ..."
                            value={search}
                        />
                    </View>

                </View>

                <FlatList
                    data={data}
                    keyExtractor={(item, index) => {
                        return index.toString();
                    }}
                    renderItem={({ item, index }) => {

                        return (
                            <ItemUser key={index} item={item} />
                        )
                    }

                    }
                />


            </ScrollView>

        </Fragment>

    )
}
const styles = StyleSheet.create({
    container: {
        height: 50,
        margin: 15,
        borderColor: "#C6C6C6",
        borderRadius: 25,
        flexDirection: "row",
        alignItems: "center",
        borderWidth: 1
    },
    containerSearch: {
        width: "20%",
        height: "100%",
        justifyContent: "center",
        alignItems: "center"
    },
    icon: {
        width: 20,
        height: 20
    },
    containerInput: {
        width: "80%",
        height: "100%",
        justifyContent: "center"
    }

});