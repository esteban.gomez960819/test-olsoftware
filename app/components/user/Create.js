import React, { useState } from 'react'
import {
    View,
    Text,
    Dimensions,
    TextInput,
    ScrollView,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import { TabNavigator } from './../TabNavigator'
import { Header } from './../Header'


const Dwidth = Dimensions.get("window").width
const Dheight = Dimensions.get("window").height
export const Create = (props) => {
    const [name, setname] = useState(null);
    const [lastName, setlastName] = useState(null);
    const [age, setage] = useState(null);
    const [position, setposition] = useState(null);


    return (
        <View style={styles.container}>
            <Header name={"Crear un usuario"} />
            <ScrollView>
                <View style={styles.containerImage}>
                    <View style={styles.containerTextImage}>
                        <Text style={styles.textImage}> Suba una</Text>
                        <Text style={styles.textImage}> imagen</Text>
                    </View>
                    <View style={styles.containerTextImagePosition}>
                        <Text style={styles.containerTextPosition}>CEO</Text>
                    </View>
                </View>

                <View style={styles.containerForm}>
                    <View style={styles.subContainerForm}>
                        <Text style={styles.text}>Nombre</Text>
                        <TextInput
                            style={styles.inputName}
                            onChangeText={text => setname(text)}
                            placeholderTextColor="#D5D5D5"

                            value={name}
                        />
                        <Text style={styles.text}>Apellido</Text>
                        <TextInput
                            style={styles.lastnameInput}
                            onChangeText={text => setlastName(text)}
                            placeholderTextColor="#D5D5D5"

                            value={lastName}
                        />
                        <Text style={styles.text}>Edad</Text>
                        <TextInput
                            style={styles.inputAge}
                            onChangeText={text => setage(text)}
                            placeholderTextColor="#D5D5D5"

                            value={age}
                        />
                        <Text style={styles.text}>Cargo</Text>
                        <TextInput
                            style={styles.inputPosition}
                            onChangeText={text => setposition(text)}
                            placeholderTextColor="#D5D5D5"

                            value={position}
                        />

                        <TouchableOpacity style={styles.button}>
                            <Text style={styles.textButton}>
                                Crear
                                </Text>
                        </TouchableOpacity>

                    </View>
                </View>
            </ScrollView>
            <TabNavigator />
        </View >
    )
}
const styles = StyleSheet.create({
    container: {
        width: Dwidth,
        height: Dheight,
        justifyContent: "center",
        backgroundColor: "#FFF"
    },
    containerImage: { justifyContent: "center", alignItems: "center", paddingTop: 35 },
    containerTextImage: { height: 200, width: 200, borderColor: "#BFBFBF", borderWidth: 1, backgroundColor: "#BFBFBF", borderRadius: 999, justifyContent: "center", alignItems: "center" },
    textImage: { color: "#949494", fontSize: 15 },
    containerTextImagePosition: { paddingTop: 15, paddingBottom: 15 },
    containerTextPosition: { fontSize: 35, color: "#707070" },
    containerForm: { width: "100%", justifyContent: "center", alignItems: "center" },
    subContainerForm: { width: "80%", },
    text: { fontSize: 20 },
    inputName: {
        paddingLeft: 15,
        height: 45,
        width: "100%",
        borderColor: '#D5D5D5',
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        marginBottom: 7
    },
    lastnameInput: {
        paddingLeft: 15,
        height: 45,
        width: "100%",
        borderColor: '#D5D5D5',
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        marginBottom: 7
    },
    inputAge: {
        paddingLeft: 15,
        height: 45,
        width: "100%",
        borderColor: '#D5D5D5',
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        marginBottom: 7
    },
    inputPosition: {
        paddingLeft: 15,
        height: 45,
        width: "100%",
        borderColor: '#D5D5D5',
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        marginBottom: 7
    },
    button: {
        backgroundColor: "#FF4E4E",
        justifyContent: "center",
        alignItems: "center",
        borderColor: "#FF4E4E",
        height: 60,
        width: "100%",
        borderWidth: 1,
        borderRadius: 10,
        marginTop: 27,
        marginBottom: 30
    },
    textButton: { color: "#FFF" }

});