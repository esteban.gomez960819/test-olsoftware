import React from 'react';
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import { useNavigation } from "@react-navigation/native";

export const ItemUser = ({ item }) => {
    const navigation = useNavigation();
    return (
        <View style={styles.container}>
            <View style={styles.containerImag}>
                <Image
                    style={styles.image}
                    source={{
                        uri: item.image,
                    }}
                />
            </View>


            <View style={styles.containerDate}>
                <Text style={styles.textName}>{item.name} {item.lastName}</Text>
                <Text style={styles.textPosition}>{item.position}</Text>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate("details", { item: item })} style={styles.containerIcon}>
                <Image
                    style={styles.ico}
                    source={require('../../assets/imag/Flecha.png')}
                />
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        height: 60,
        flexDirection: "row",
        margin: 10,
        marginLeft: 25,
        marginRight: 40
    },
    containerImag: {
        height: 65,
        width: 65,
        borderRadius: 200,
        justifyContent: "center",
        alignItems: "center"
    },
    image: {
        width: "100%",
        height: "100%",
        borderRadius: 200,
    },
    containerDate: {
        width: "70%",
        paddingLeft: 5,
        margin: 5
    },
    textName: {
        fontSize: 18,
        color: "#171515"
    },
    textPosition: {
        fontSize: 14,
        color: "#707070"
    },
    containerIcon: {
        width: "15%",
        justifyContent: "center",
        alignItems: "center"
    },
    ico: {
        width: 20,
        height: 20
    }

});