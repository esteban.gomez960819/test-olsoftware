import React, { useEffect, useState } from 'react'
import {
    View,
    Text,
    Dimensions,
    TextInput,
    ScrollView,
    TouchableOpacity,
    Image,
    StyleSheet
} from 'react-native';
import { TabNavigator } from './../TabNavigator'
import { Header } from './../Header'


const Dwidth = Dimensions.get("window").width
const Dheight = Dimensions.get("window").height
export const Details = (props) => {
    const [name, setname] = useState(null);
    const [lastName, setlastName] = useState(null);
    const [age, setage] = useState(null);
    const [position, setposition] = useState(null);
    const [image, setimage] = useState(null);

    useEffect(() => {
        if (props.route.params.item) {
            const { name, lastName, age, position, image } = props.route.params.item
            setname(name)
            setlastName(lastName)
            setage(age.toString())
            setposition(position)
            setimage(image)
        }
    }, [])
    useEffect(() => {
        if (props.route.params.item) {
            const { name, lastName, age, position, image } = props.route.params.item
            setname(name)
            setlastName(lastName)
            setage(age.toString())
            setposition(position)
            setimage(image)
        }
    }, [props.route.params.item])

    return (
        <View style={styles.container}>
            <Header isback={true} name={"Detalle usuario"} />
            <ScrollView>
                <View style={styles.containerImage}>
                    <View style={{}}>
                        <Image
                            style={styles.image}
                            source={{
                                uri: image,
                            }}
                        />
                    </View>
                    <View style={styles.containerPosition}>
                        <Text style={styles.textPosition}>{position}</Text>
                    </View>
                </View>

                <View style={styles.containerForm}>
                    <View style={styles.subContainerForm}>
                        <Text style={styles.text}>Nombre</Text>
                        <TextInput
                            style={styles.inputName}
                            onChangeText={text => setname(text)}
                            placeholderTextColor="#D5D5D5"
                            placeholder="Nombre"
                            value={name}
                        />
                        <Text style={styles.text}>Apellido</Text>
                        <TextInput
                            style={styles.inputLastname}
                            onChangeText={text => setlastName(text)}
                            placeholderTextColor="#D5D5D5"
                            placeholder="Apellido"
                            value={lastName}
                        />
                        <Text style={styles.text}>Edad</Text>
                        <TextInput
                            style={styles.inputAge}
                            onChangeText={text => setage(text)}
                            placeholderTextColor="#D5D5D5"
                            placeholder="Edad"
                            value={age}
                        />
                        <Text style={styles.text}>Cargo</Text>
                        <TextInput
                            style={styles.inputPosition}
                            onChangeText={text => setposition(text)}
                            placeholderTextColor="#D5D5D5"
                            placeholder="Cargo"
                            value={position}
                        />

                        <TouchableOpacity style={styles.buttonUpdate}>
                            <Text style={styles.textUpdate}>
                                Actualizar
                                </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.buttonDelete}>
                            <Text style={styles.textDelete}>Eliminar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
            <TabNavigator />
        </View >
    )
}

const styles = StyleSheet.create({
    container: { width: Dwidth, height: Dheight, justifyContent: "center", backgroundColor: "#FFF" },
    containerImage: { justifyContent: "center", alignItems: "center", paddingTop: 35 },
    image: { height: 200, width: 200, borderRadius: 999 },
    containerPosition: { paddingTop: 15, paddingBottom: 15 },
    textPosition: { fontSize: 35, color: "#707070" },
    containerForm: { width: "100%", justifyContent: "center", alignItems: "center" },
    subContainerForm: { width: "80%", },
    text: { fontSize: 20 },
    inputName: {
        paddingLeft: 15,
        height: 45,
        width: "100%",
        borderColor: '#D5D5D5',
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        marginBottom: 7
    },
    inputLastname: {
        paddingLeft: 15,
        height: 45,
        width: "100%",
        borderColor: '#D5D5D5',
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        marginBottom: 7
    },
    inputAge: {
        paddingLeft: 15,
        height: 45,
        width: "100%",
        borderColor: '#D5D5D5',
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        marginBottom: 7
    },
    inputPosition: {
        paddingLeft: 15,
        height: 45,
        width: "100%",
        borderColor: '#D5D5D5',
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        marginBottom: 7
    },
    buttonUpdate: {
        backgroundColor: "#FF4E4E",
        justifyContent: "center",
        alignItems: "center",
        borderColor: "#FF4E4E",
        height: 60,
        width: "100%",
        borderWidth: 1,
        borderRadius: 10,
        marginTop: 27,
        marginBottom: 7
    },
    textUpdate: { color: "#FFF" },
    buttonDelete: {
        backgroundColor: "#FFF",
        justifyContent: "center",
        alignItems: "center",
        borderColor: "#FF4E4E",
        height: 60,
        width: "100%",
        borderWidth: 1,
        borderRadius: 10,
        marginTop: 17,
        marginBottom: 27
    },
    textDelete: { color: "#FF4E4E" }




})