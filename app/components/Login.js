import React, { useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    Image,
    Dimensions,
    TextInput,
    TouchableOpacity
} from 'react-native';
import { useNavigation } from "@react-navigation/native";


const Dwidth = Dimensions.get("window").width
const Dheight = Dimensions.get("window").height
export const Login = () => {
    const [user, setUser] = useState('');
    const [pass, setPass] = useState('');

    const navigation = useNavigation();


    return (
        <SafeAreaView>
            <ScrollView>
                <View style={styles.main}>
                    <View style={styles.containerImag}>
                        <Image
                            style={styles.imag}
                            source={require('../assets/imag/Logo.png')}
                        />
                    </View>
                    <View style={styles.containerLogin}>
                        <View style={styles.sessionOne}>
                            <TextInput
                                style={styles.textInput}
                                onChangeText={text => setUser(text)}
                                placeholderTextColor="#D5D5D5"
                                placeholder="Usuario"
                                value={user}
                            />
                            <TextInput
                                style={styles.textInput}
                                onChangeText={text => setPass(text)}
                                placeholderTextColor="#D5D5D5"
                                placeholder="Contraseña"
                                secureTextEntry={true}
                                value={pass}
                            />
                            <TouchableOpacity onPress={() => navigation.navigate("home")} style={styles.buttonLogin}>
                                <Text style={styles.textLogin}>
                                    Iniciar sesión
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.buttonRegister}>
                                <Text style={styles.textRegister}>Registrarse</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.sessionTwo}><Text style={styles.textSessionTwo}>Powerared by OLSoftware</Text></View>
                    </View>
                    <View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView >

    )


}

const styles = StyleSheet.create({
    buttonLogin: {
        backgroundColor: "#FF4E4E",
        justifyContent: "center",
        alignItems: "center",
        borderColor: "#FF4E4E",
        height: 60,
        width: "100%",
        borderWidth: 1,
        borderRadius: 10,
        marginTop: 7,
        marginBottom: 7
    },
    buttonRegister: {
        backgroundColor: "#FFF",
        justifyContent: "center",
        alignItems: "center",
        borderColor: "#FF4E4E",
        height: 60,
        width: "100%",
        borderWidth: 1,
        borderRadius: 10,
        marginTop: 7,
        marginBottom: 7
    },
    textRegister: {
        color: "#FF4E4E"
    },
    textLogin: {
        color: "#FFF"
    },
    main: {
        width: Dwidth,
        height: Dheight,
        alignItems: "center"
    },
    containerImag: {
        width: Dwidth,
        height: Dheight / 2.5,
        justifyContent: "center",
        alignItems: "center",
        paddingTop: 15,
        paddingBottom: 15
    },
    imag: {
        height: 200,
        width: 150
    },
    containerLogin: {
        height: Dheight - Dheight / 2.5,
        justifyContent: "space-between",
        alignItems: "center",
    },
    textInput: {
        paddingLeft: 15,
        height: 45,
        width: "100%",
        borderColor: '#D5D5D5',
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        marginBottom: 7
    },
    sessionOne: {
        width: Dwidth / 1.2
    },
    sessionTwo: {
        marginBottom: 15
    },
    textSessionTwo: {
        fontFamily: "Roboto",
        fontSize: 18
    }

});



