import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import Users from "../components/user/Users";
import Details from "../components/user/Details";
import Create from "../components/user/Create";


const Stack = createStackNavigator();

export const HomeStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="users"
                component={Users}
                options={{ title: "Usuarios" }}
            />
            <Stack.Screen
                name="details"
                component={Details}
                options={{ title: "Detalle de usuario" }}
            />

            <Stack.Screen
                name="create"
                component={Create}
                options={{ title: "Crear usuario" }}
            />
        </Stack.Navigator>
    );
}
