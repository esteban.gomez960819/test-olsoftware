import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Home } from './../components/user/Home'
import { Details } from './../components/user/Details'
import { Login } from './../components/Login'
import { Create } from '../components/user/Create';

const Stack = createStackNavigator();

export const Navigation = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="login">
                <Stack.Screen options={{ headerShown: false }} name="login" component={Login} />
                <Stack.Screen options={{
                    headerLeft: null, headerTitleStyle: { alignSelf: 'center' }, headerShown: false
                }} name="home" component={Home} />
                <Stack.Screen options={{
                    headerLeft: null, headerTitleStyle: { alignSelf: 'center' }, headerShown: false
                }} name="details" component={Details} />
                <Stack.Screen options={{
                    headerLeft: null, headerTitleStyle: { alignSelf: 'center' }, headerShown: false
                }} name="create" component={Create} />
            </Stack.Navigator>

        </NavigationContainer>
    )
}


